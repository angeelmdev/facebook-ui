import 'package:device_preview/device_preview.dart';
import 'package:facebook_iu/facebook_ui/facebook_ui.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    DevicePreview(
      builder: (_) => const MyApp(),
      enabled: !kReleaseMode,
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      useInheritedMediaQuery: true,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      home: const FacebookUi(),
      theme: ThemeData(
        fontFamily: 'Nunito',
      ),
    );
  }
}
