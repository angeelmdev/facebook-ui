import 'package:facebook_iu/icons/custom_icons.dart';
import 'package:flutter/material.dart';
import 'circle_button.dart';

class QuickActions extends StatelessWidget {
  const QuickActions({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: FittedBox(
        fit: BoxFit.scaleDown,
        alignment: Alignment.centerLeft,
        child: Row(
          children: [
            _QuickButton(
              color: Color(0xFF94C390),
              iconData: CustomIcons.photos,
              label: "Gallery",
            ),
            SizedBox(width: 15),
            _QuickButton(
              color: Color(0xFF218BF2),
              iconData: CustomIcons.user_friends,
              label: "Tag Friends",
            ),
            SizedBox(width: 15),
            _QuickButton(
              color: Color(0xFFF36E6D),
              iconData: CustomIcons.video_camera,
              label: "Live",
            ),
          ],
        ),
      ),
    );
  }
}

class _QuickButton extends StatelessWidget {
  final IconData iconData;
  final Color color;
  final String label;

  const _QuickButton({
    required this.label,
    required this.iconData,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        right: 25,
      ),
      decoration: BoxDecoration(
        color: color.withOpacity(0.2),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          CircleButton(
            color: color.withOpacity(0.6),
            iconData: iconData,
          ),
          const SizedBox(width: 15),
          Text(
            label,
            style: TextStyle(
              color: color,
            ),
          ),
        ],
      ),
    );
  }
}
